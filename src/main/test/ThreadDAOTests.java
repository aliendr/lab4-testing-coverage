import com.hw.db.DAO.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests{

    @Test
    @DisplayName("Tree sort full statement coverage test #1")
    void treeSortTest1() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO t = new ThreadDAO(mJdbc);

        ThreadDAO.treeSort(1, null, 50, false);
        Mockito.verify(mJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(),
                Mockito.anyInt()
        );
    }

    @Test
    @DisplayName("Tree sort full statement coverage test #2")
    void treeSortTest2() {
        JdbcTemplate mJdbc = Mockito.mock(JdbcTemplate.class);
        ThreadDAO t = new ThreadDAO(mJdbc);

        ThreadDAO.treeSort(1, 100, 50, true);
        Mockito.verify(mJdbc).query(
                Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.anyInt(),
                Mockito.anyInt(),
                Mockito.anyInt()
        );
    }
}