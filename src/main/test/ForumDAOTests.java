import com.hw.db.DAO.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTests {

    private JdbcTemplate mockJdbc;

    @BeforeEach
    void PreTestBehaviour() {
        mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
    }

    @Test
    @DisplayName("List of users with no agruments")
    void userListTest1() {
        ForumDAO.UserList("slug",null, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000")
    void userListTest2() {
        ForumDAO.UserList("slug",null, "1000", false);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000 desc")
    void userListTest3() {
        ForumDAO.UserList("slug",null, "1000", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users limit 10")
    void userListTest4() {
        ForumDAO.UserList("slug",10, null, null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000 desc limit 10")
    void userListTest5() {
        ForumDAO.UserList("slug",10, "1000", true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users desc")
    void userListTest6() {
        ForumDAO.UserList("slug",null, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users since 1000 limit 10")
    void userListTest7() {
        ForumDAO.UserList("slug",10, "1000", null);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    @DisplayName("List of users limit 10 desc")
    void userListTest8() {
        ForumDAO.UserList("slug",10, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}